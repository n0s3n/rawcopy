################# Read through all CEL files and import SNPs

library(affxparser)

## fixa setwd mm

cdf=readCdfCellIndices('GenomeWideSNP_6.cdf')
cdf=readCdfCellIndices('CytoScanHD_Array.cdf')
# cdf=readCdfCellIndices('/media/safe/bjorn/git/rawcopy/rawcopyReference/data/GenomeWideSNP_6.cdf')
# cdf=readCdfCellIndices('/media/safe/bjorn/git/rawcopy/rawcopyReference/data/CytoScanHD_Array.cdf')
#load('annot.Rdata')


probeNames <- names(cdf)
snpIx <- grep('SNP',as.character(probeNames))
snpIx <- grep('^S',as.character(probeNames))
a <- b <- snpIx #to be filled

sampleData=data.frame(sample=dir()[grep('CEL',dir())])

#sampleData$snp.nsp=NA
#sampleData$snp.sty=NA
#sampleData$snp.both=NA
#sampleData$snp.X=NA
#sampleData$snp.Y=NA
#sampleData$snp.MT=NA
#sampleData$probe.nsp=NA
#sampleData$probe.sty=NA
#sampleData$probe.both=NA
#sampleData$probe.X=NA
#sampleData$probe.Y=NA
#sampleData$probe.MT=NA
#save(sampleData,file='sampleData.Rdata')
#load("sampleData.Rdata")
snps <- data.frame(Name=probeNames[snpIx], ix=1:length(snpIx))

# annot <- merge(annot,data.frame(Name=probeNames[snpIx], ix=1:length(snpIx)),by=1,all=F)
# trim <- function(vector, n=1, end=NULL) {
#     length=length(vector)    
#     if (is.null(end)) end=n   
#     return(vector[(n+1):(length-end)])
# }

for (s in 1:nrow(sampleData)) {
    celfile=readCel(as.character(sampleData$sample[s]))
    intensities <- matrix(celfile$intensities, nrow=2572, ncol=2680)
    rm(celfile)
    cat(s,'\n')
    for (i in 1:length(a)) {
        ix1 <- cdf[[snpIx[i]]][[1]][[1]][[1]]
        ix2 <- cdf[[snpIx[i]]][[1]][[2]][[1]]
        ta <- (intensities[ix1])
        tb <- (intensities[ix2])
        a[i] <- round(2^mean(log2(ta)))
        b[i] <- round(2^mean(log2(tb)))
    }
    snps[[paste('a',s,sep='.')]] <- a
    snps[[paste('b',s,sep='.')]] <- b
    rm(intensities)
}    
save(snps,file=paste('CytoScanHD_snps_',format(Sys.time(), "%F-%H_%M_%S"),'.Rdata',sep=''))


### and for copy probes:::

library(affxparser)
# cdf=readCdfCellIndices('/media/safe/bjorn/git/rawcopy/rawcopyReference/data/GenomeWideSNP_6.cdf')
# cdf=readCdfCellIndices('/media/safe/bjorn/git/rawcopy/rawcopyReference/data/CytoScanHD_Array.cdf')
cdf=readCdfCellIndices('GenomeWideSNP_6.cdf')
cdf=readCdfCellIndices('CytoScanHD_Array.cdf')
sampleData=data.frame(sample=dir()[grep('CEL',dir())])

probeNames <- names(cdf)
probeIx <- grep('^CN',as.character(probeNames)) #SNP 6.0
probeIx <- grep('^C',as.character(probeNames)) #CytoScanHD
value <- probeIx #to be filled
# 
probes <- data.frame(Name=probeNames[probeIx], ix=1:length(probeIx))

# 
for (s in 1:nrow(sampleData)) {
    celfile=readCel(as.character(sampleData$sample[s]))
    intensities <- matrix(celfile$intensities, nrow=2572, ncol=2680)
    rm(celfile)
    cat(s,'\n')
    for (i in 1:length(value)) {
        ix <- cdf[[probeIx[i]]][[1]][[1]][[1]]
        value[i] <- log2(median(intensities[ix]))
    }
    probes[[paste('v',s,sep='.')]] <- value
    rm(intensities)
}    
## !!! save probes here.
save(probes,file=paste('CytoScanHD_probes_',format(Sys.time(), "%F-%H_%M_%S"),'.Rdata',sep=''))
