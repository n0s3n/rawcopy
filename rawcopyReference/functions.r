segMatch <- function(segments.txt,probes.txt) {
    ## Returns a data frame with probes.startIx probes.endIx
    #verry fast
    
    segStarts=data.frame(n.seg=1:nrow(segments.txt),n.pos=NA,chr=chrom_n(segments.txt$Chromosome),pos=segments.txt$Start,type='start')
    segEnds=data.frame(n.seg=1:nrow(segments.txt),n.pos=NA,chr=chrom_n(segments.txt$Chromosome),pos=segments.txt$End,type='end')
    lpos=data.frame(n.seg=NA,n.pos=1:nrow(probes.txt),chr=chrom_n(probes.txt$Chromosome),pos=(probes.txt$Start+probes.txt$End)/2,type='logr')
    dummy1=data.frame(n.seg=NA,n.pos=NA,chr=-Inf,pos=-Inf,type='logr')
    dummy2=data.frame(n.seg=NA,n.pos=NA,chr=Inf,pos=Inf,type='logr')
    
    table=rbind(segStarts,segEnds,lpos,dummy1,dummy2)
    table=table[order(table$chr,table$pos),]
     
    start=(table$type=='start')
    end=(table$type=='end')
    #l=(table$type=='logr')

    # omitting segment ends, let segment start "n.pos" be that of the next marker
    table$n.pos[!end][start[!end]] <- table$n.pos[!end][which(start[!end])+1]
    
    # omitting segment starts, let segment end "n.pos" be that of the previous marker
    table$n.pos[!start][end[!start]] <- table$n.pos[!start][which(end[!start])-1]
    
    # re-create a data frame with startIx and endIx (in probes.txt) per segment
    indices=data.frame(startIx=table$n.pos[start],endIx=table$n.pos[end])
    
    na=is.na(indices$startIx+indices$endIx)
    na[!na] = indices$startIx[!na]>indices$endIx[!na]
    indices$startIx[na] <- indices$endIx[na] <- NA
    
    return(indices)
}

getAI <- function(baf,min=20) {#,divide=T) {
    baf=baf[!is.na(baf)]
    if (length(baf)<min) return(c(NA,NA))
    baf=2*abs(baf-0.5)
    km=kmeans(x=baf,centers=2)
    het=min(km$centers)
    hom=max(km$centers)
    if (min(km$size)<5) return (c(NA,NA))
    #if (divide) return(het/hom) else return(het)
    return(c(het,hom))
}
getLAF <- function(baf,min=100) { ### Inte klar! 
    baf=baf[!is.na(baf)]
    if (length(baf)<min) return(NA)
    d=density(baf,adjust=0.5)
    t=c(F,diff(sign(diff(d$y)))<0,F)
    t[is.na(t)]=F; t=which(t)
    hom_height=mean(d$y[t[1]],d$y[t[length(t)]])
    t=trim(t)
    peaks=d$x[t]
    peaks=peaks[d$y[t]>=0.2*hom_height]
    peaks=(0.5-abs(peaks-0.5))
    if (length(peaks)==0) return(0)
    if (length(peaks)==1) return(0.5)
    if (length(peaks)>=2) return(mean(peaks))
}

addGenes <- function(data,genes) {
    if (nrow(data)==0) return(data)
    data$genes=''
    for (i in 1:nrow(data)) {
        ix=which(genes$chrom==as.character(data$Chromosome[i]) & genes$txStart<data$End[i] & genes$txEnd>data$Start[i])
        if (length(ix)>0) data$genes[i]=paste(sort(unique(genes$name2[ix])),collapse=', ')
    }
    return(data)
}

chrom_ucsc <- function(data) {
  out=rep(NA,length(data))
  c=as.character(unique(data))
  for (i in 1:length(c)) {
    temp=c[i]
    if (temp=='23') temp='X'
    if (temp=='24') temp='Y'
    if (temp=='MT') temp='M'
    if (temp %in% c(1:22,'X','Y','M')) out[data==c[i]]=paste('chr',temp, sep='')
  }
  return(out)
}

chrom_n <- function(data) {
  out=rep(Inf,length(data))
  for (c in c(1:22)) {
    out[data==paste('chr',c, sep='')]=c
  }
  out[data=='chrX']=23
  out[data=='chrY']=24
  out[data=='chrM']=25
  return(out)
}

## Funktion för samband mellan hybridisering per snp och rawBAF.
curve <- structure(list(x = c(7, 7.1, 7.2, 7.3, 7.4, 7.5, 7.6, 7.7, 7.8, 
                                  7.9, 8, 8.1, 8.2, 8.3, 8.4, 8.5, 8.6, 8.7, 8.8, 8.9, 9, 9.1, 
                                  9.2, 9.3, 9.4, 9.5, 9.6, 9.7, 9.8, 9.9, 10, 10.1, 10.2, 10.3, 
                                  10.4, 10.5, 10.6, 10.7, 10.8, 10.9, 11, 11.1, 11.2, 11.3, 11.4, 
                                  11.5, 11.6, 11.7, 11.8, 11.9, 12, 12.1, 12.2, 12.3, 12.4, 12.5, 
                                  12.6, 12.7, 12.8, 12.9, 13, 13.1, 13.2, 13.3, 13.4, 13.5, 13.6, 
                                  13.7, 13.8, 13.9, 14), y = c(0.0416335227664092, 0.050979763640533, 
                                                               0.0603961238329525, 0.0699257007683986, 0.0796069246141624, 0.089472319342666, 
                                                               0.0995474941172804, 0.109850392141516, 0.120390812524894, 0.13117020888403, 
                                                               0.142181756918978, 0.153410672653813, 0.164834753905488, 0.176425110239248, 
                                                               0.188147041446085, 0.199961021562655, 0.21182374463445, 0.223689189665498, 
                                                               0.235509665271151, 0.247236799154316, 0.258822443319578, 0.270219472568468, 
                                                               0.281382460931114, 0.292268227947705, 0.302836253800124, 0.313048968911182, 
                                                               0.322871929495096, 0.332273895395454, 0.341226830146697, 0.349705845339663, 
                                                               0.357689111913343, 0.365157759865717, 0.37209578511123, 0.378489977969992, 
                                                               0.384329882346757, 0.389607788468662, 0.394318755630878, 0.398460655348789, 
                                                               0.402034220248617, 0.405043080508809, 0.407493768136766, 0.409395670095172, 
                                                               0.410760914320152, 0.411604177795414, 0.411942412619328, 0.411794493781286, 
                                                               0.411180800367166, 0.410122749303893, 0.40864230673061, 0.406761505977464, 
                                                               0.404502002477085, 0.40188469452177, 0.398929434687548, 0.395654850324527, 
                                                               0.392078283345505, 0.388215850385281, 0.384082615093144, 0.379692855703378, 
                                                               0.375060403862655, 0.370199025585281, 0.365122812558449, 0.359846551998482, 
                                                               0.354386045797419, 0.348758354510559, 0.342981948351701, 0.337076755183574, 
                                                               0.331064103842254, 0.324966569326709, 0.318807733770764, 0.312611883139102, 
                                                               0.306403663826835)), .Names = c("x", "y"), row.names = c(NA, 
                                                                                                                        -71L), class = "data.frame")

curve_old <- structure(list(x = c(7, 7.1, 7.2, 7.3, 7.4, 7.5, 7.6, 7.7, 7.8, 
                              7.9, 8, 8.1, 8.2, 8.3, 8.4, 8.5, 8.6, 8.7, 8.8, 8.9, 9, 9.1, 
                              9.2, 9.3, 9.4, 9.5, 9.6, 9.7, 9.8, 9.9, 10, 10.1, 10.2, 10.3, 
                              10.4, 10.5, 10.6, 10.7, 10.8, 10.9, 11, 11.1, 11.2, 11.3, 11.4, 
                              11.5, 11.6, 11.7, 11.8, 11.9, 12, 12.1, 12.2, 12.3, 12.4, 12.5, 
                              12.6, 12.7, 12.8, 12.9, 13, 13.1, 13.2, 13.3, 13.4, 13.5, 13.6, 
                              13.7, 13.8, 13.9, 14), 
                        y = c(0.0416335227664092, 0.0495511922119616, 
                              0.0575389809758096, 0.0656399864826843, 0.0738926388998767, 0.0823294621998089, 
                              0.0909760655458518, 0.0998503921415163, 0.108962241096323, 0.118313066026887, 
                              0.127896042633264, 0.137696386939527, 0.147691896762631, 0.157853681667819, 
                              0.168147041446085, 0.178532450134084, 0.188966601777307, 0.199403475379784, 
                              0.209795379556865, 0.220093942011459, 0.230251014748149, 0.240219472568468, 
                              0.249953889502543, 0.259411085090562, 0.26855053951441, 0.277334683196896, 
                              0.285729072352239, 0.293702466824025, 0.301226830146697, 0.308277273911092, 
                              0.3148319690562, 0.320872045580003, 0.326381499396944, 0.331347120827135, 
                              0.335758453775328, 0.339607788468662, 0.342890184202307, 0.345603512491646, 
                              0.347748505962903, 0.349328794794523, 0.350350910993909, 0.350824241523743, 
                              0.350760914320152, 0.350175606366843, 0.349085269762185, 0.347508779495572, 
                              0.34546651465288, 0.342979892161036, 0.340070878159181, 0.336761505977464, 
                              0.333073431048514, 0.329027551664627, 0.324643720401834, 0.319940564610241, 
                              0.314935426202648, 0.309644421813852, 0.304082615093144, 0.298264284274807, 
                              0.292203261005512, 0.285913311299567, 0.279408526844163, 0.272703694855625, 
                              0.26581461722599, 0.258758354510559, 0.25155337692313, 0.244219612326431, 
                              0.23677838955654, 0.229252283612423, 0.221664876627907, 0.214040454567673, 
                              0.206403663826835)), .Names = c("x", "y"), row.names = c(NA, 
                                                                                       -71L), class = "data.frame")

bm <- function(X, shift=0,curve) {
  X=X-shift
  X=round(X,1)
  Y=curve$y[match(X,curve$x)]
  return(Y)
}

## Används i makeReferenceData för euklidiska avstånd mellan punkter och kurva
getResiduals <- function(xpoints, ypoints, func, xmin=7, xmax=13) {
    xvals=seq(xmin,xmax,0.1)
    yvals=func(xvals)
    dists=rep(NA,length(xpoints))
    for (i in 1:length(dists)) dists[i]=min(sqrt( (xpoints[i]-xvals)^2+(ypoints[i]-yvals)^2 ) ,na.rm=T)
    return(dists)
}


getSqrt <- function(snpix,sampleix) {
    return(round(log2(median(sqrt(snps[snpix,ixa[sampleix]]^2+snps[snpix,ixb[sampleix]]^2))),2))
}

outliers <- function(ta, tb, cut=0.03) {
    r <- sqrt(ta^2+tb^2)
    q <- abs(1-r/median(r))
    return(q>cut)
}

## Function for regionMean calculation (fragment GC content).
regionMeans <- function(s,e,pos,values) {
    means <- rep(NA,length(s))
    temp=data.frame(pos=c(pos,s,e),value=c(values,rep(NA,2*length(s))))
    order=order(order(as.integer(temp$pos))) #order: vart i sorteradtemp sitter respektive värde i temp
    spositions=order[(length(pos)+1) : (length(pos)+length(s))] # vart i sorterad temp sitter respektive "s"
    epositions=order[(length(pos)+1+length(s)) : nrow(temp)]
    temp=temp[order(as.integer(temp$pos)),2] #temp är numera bara sorterade values, med NA för de pos som är s eller e
    for (i in 1:length(means)) {
        means[i]=mean(temp[spositions[i]:epositions[i]],na.rm=T)
    }
    return(means)
}

## Function for normalization
norm <- function(data, factor, window=0.05,trim=1) {
    n=length(data)
    range1=c(round(seq(1,n,window*n)),n)
    o1=order(factor1)
    data2=data
    for (i in 2:length(range1)) {
        ix1=o1[range1[i-1]:range1[i]]
        ix1=ix1[!is.na(ix1)]
        d=data[ix1]
        if (length(d)>2*trim) mid=mean(d[order(d)[(1+trim):(length(d)-trim)]],na.rm=T) else mid=mean(d,na.rm=T)
        if (length(ix1)>1) data2[ix1]=d-mid
    }
    return(data2)
}
norm2 <- function(data, factor1, factor2, window=0.05,trim=1) {
    n=length(data)
    range1=c(round(seq(1,n,window*n)),n)
    range2=c(round(seq(1,n,window*n)),n)
    o1=order(factor1)
    o2=order(factor2)
    data2=data
    for (i in 2:length(range1)) {
        ix1=o1[range1[i-1]:range1[i]]
        ix1=ix1[!is.na(ix1)]
        for (j in 2:length(range2)) {
            ix2=o2[range2[j-1]:range2[j]]
            ix2=ix2[!is.na(ix2)]
            ix12 = intersect(ix1, ix2)
            d=data[ix12]
            if (length(d)>2*trim) mid=mean(d[order(d)[(1+trim):(length(d)-trim)]],na.rm=T) else mid=mean(d,na.rm=T)
            if (length(ix12)>1) data2[ix12]=d-mid
        }
    }
    return(data2)
}
norm3 <- function(data, factor1, factor2, factor3, window=0.05,trim=1) {
    n=length(data)
    range1=c(round(seq(1,n,window*n)),n)
    range2=c(round(seq(1,n,window*n)),n)
    range3=c(round(seq(1,n,window*n)),n)
    o1=order(factor1)
    o2=order(factor2)
    o3=order(factor3)
    data2=data
    for (i in 2:length(range1)) {
        #print(i)
        #browser()
        ix1=o1[range1[i-1]:range1[i]]
        ix1=ix1[!is.na(ix1)]
        for (j in 2:length(range2)) {
            ix2=o2[range2[j-1]:range2[j]]
            ix2=ix2[!is.na(ix2)]
            ix12 = intersect(ix1, ix2)
            for (k in 2:length(range3)) {
                ix3=o3[range3[k-1]:range3[k]]
                ix3=ix3[!is.na(ix3)]
                ix123 = intersect(ix12, ix3)
                d=data[ix123]
                #browser()
                if (length(d)>2*trim) mid=mean(d[order(d)[(1+trim):(length(d)-trim)]],na.rm=T) else mid=mean(d,na.rm=T)
                if (length(ix123)>1) data2[ix123]=d-mid
                #if (i==5 & j==5) browser()
            }
        }
    }
    return(data2)
}

## Remove first and last few values of a vector
trim <- function(vector, n=1, end=NULL) {
    length=length(vector)    
    if (is.null(end)) end=n   
    return(vector[(n+1):(length-end)])
}

